/*Here, we will be learning how to do styling in react using css-> typestyle. 
For this, install typestyle by using cmd: npm install typestyle --save 
Also, in this basically we will learn  how to mk custom css inside one java script file in which js code and css code. This can be done using typestyling. Import style from typeStyle*/
/*One way to use css in react is to create a separate css page and then import that to your react js page. Another way  is, to put css code in the same react js page using typestyle */
/*We will implement both the methods */
/*typeStyle is a library used for implementing advanced css in react. Through this we can keep all 
js code and css code in one file-> for selectors, applying styles, hover, nested classes, 
psudo classes */
import React from'react';
import ReactDOM from 'react-dom';
import {style} from 'typestyle';  //--->Way2
//import './counter.css' //---> Way1


/*Make css classes here--->Way2
We will mk two stateless component classes for this. Can write style like this only or 
directly style without paranthesis. These stateless fxnl classes will tk objects-> key:value 
pair and values if string shld be enclosed within quotes and also key shld be in Camelcase format. */
/*We can also add selectors in typestyle like: & */
    
    
    
    
const counter=style({
    fontSize:'1rem',
    padding:'.7rem',
    backgroundColor:'#ccc',
    display: 'inline-block',
    border: '1px solid #eee',
    $nest:{ //{} means object ->json format
   '&:hover': {backgroundColor: 'goldenrod'},//<-- json format
   '& > div':  {backgroundColor:"Red"}// if hover goldenrod color shld come if selected blue color shld cm
}

});

const btn=style({
    backgroundColor:'#222' ,
    color: 'white',
    border:'none',
    border: '1px solid #ccc',
    padding:'3rem' ,
    borderRadius:'3rem'

})


class Counter extends React.Component{
constructor(){
    super();
this.incCount=this.incCount.bind(this);
this.handleCheckboxChange=this.handleCheckboxChange.bind(this);
this.submit=this.submit.bind(this);
    this.state={
        count:0,
        checked:false, 
        textval:''
    }
}

incCount(){
    let cnt=this.state.count+1;
    this.setState({
        count:cnt
    })
}

handleCheckboxChange(){
this.setState({
    checked:!this.state.checked
})

}

submit(event){
event.preventDefault();
    
    this.setState({
        textval:this.input.value
        
    })
    console.log(this.state.textval)
    }
////<input type="checkbox"/>
render(){
    return (
        //className="counter", className="btn" are way1
        <section>
        <div className=/*"counter"*/ {counter}>{this.state.count} <div>Name:Astha</div></div>   
        
        <button className=/*"btn"*/{btn} onClick={this.incCount}>   Add     </button>
        <div> 
        <form onSubmit={this.submit}>
         <label>
          <input type="checkbox"  chkd={this.state.checked} onChange={this.handleCheckboxChange} />
            <span>Exclude</span>
        </label>  
        {this.state.checked ? 
          <input type="text" ref={(input)=>this.input=input}  />  :''}
          <button type="submit">Save</button>
          </form>
        </div>
        </section>
    )
}
}

ReactDOM.render(<Counter/>,document.getElementById('root'));
